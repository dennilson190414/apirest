package com.example.demo.model;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import java.util.Collection;

@Entity
@Getter
@Setter
public class Address {


	@Id
	@Basic
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idAddress;
	
	private String street;

    private String state;

    private String city;

    private String country;

    private String zip;
	
	@OneToMany(mappedBy = "address", fetch = FetchType.LAZY)
	private Collection<User> users;

}
