package com.example.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.example.demo.model.User;

public interface UserRepository extends CrudRepository<User, Integer>{
	
	@Query("select new map(u.idUser as id, u.name as name , u.email as email, u.birthDate as birthDate, a.country as country, a.city as city, a.street as street) from User   u inner join Address a "
			+ "on a.idAddress = u.address.id")
	 List<Object> getUserAll();


	@Query("select new map( u.name as name , a.country as country ) from User u inner join Address a on u.address.id = a.idAddress where u.idUser = ?1")
	 List<Object> getusersById(Integer idUser);
}
