package com.example.demo.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;
import javax.websocket.server.PathParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.example.demo.model.User;
import com.example.demo.service.UserServiceImplements;

@RestController
@RequestMapping("/")
@CrossOrigin("*")
public class UserController {
	
	@Autowired
	UserServiceImplements serviceImplements;
	
	private Map<String, Object> map;

    public UserController(){
        map = new HashMap<>();
    }
    
    @GetMapping("/getusers")
    public ResponseEntity<?> getusers() {
    	try {
    		List<Object>User =serviceImplements.getusers();
            map.put("error", false);
            map.put("content" , User);
            return new ResponseEntity<Map>(map, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
            map.put("error", true);
            map.put("mensaje", "Error: " + e.getMessage());
            return new ResponseEntity<Map>(map, HttpStatus.INTERNAL_SERVER_ERROR);
			
		}
    }
    

    @PostMapping("/createUsers")
    public ResponseEntity<?> createUsers(@RequestBody User user)
    {
    	try {
			map.put("error",false);
            map.put("content", serviceImplements.createUsers(user));
            return new ResponseEntity<Map>(map,HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
            map.put("error",true);
            map.put("mensaje","ERROR : " + e.getMessage());
            return new ResponseEntity<Map>(map, HttpStatus.INTERNAL_SERVER_ERROR);
		}
    }


    @PutMapping("/updateUsersById")
    public  ResponseEntity<?> updateUser ( @RequestBody User user ){
        User userU = null;
        try {
            map.put("error",false);
            map.put("content", serviceImplements.updateUsersById(user));
            return new ResponseEntity<Map>(map,HttpStatus.OK);
        }catch (Exception e){
            e.printStackTrace();
            map.put("error",true);
            map.put("mensaje","ERROR : " + e.getMessage());
            return new ResponseEntity<Map>(map, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/deleteUsersById/{id}")
    public ResponseEntity<?> deleteUsersById(@PathVariable Integer id){
        try {
            map.put("error", false);
            map.put("content", " ");
            serviceImplements.deleteUsersById(id);
            return new ResponseEntity<Map>(map,HttpStatus.OK);
        }catch (Exception e){
            e.printStackTrace();
            map.put("error", true);
            map.put("mensaje", "ERROR :" + e.getMessage());
            return new ResponseEntity<Map>(map,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/getusersById/{id}")
    public ResponseEntity<?> getusersById(@PathVariable Integer id){
        Map<String, Object> map = new HashMap<>();
        try {
            List<Object> user = serviceImplements.getusersById(id);
            map.put("error", false);
            map.put("content", user);
            return new ResponseEntity<Map>(map,HttpStatus.OK);
        }catch (Exception e){
            e.printStackTrace();
            map.put("error", true);
            map.put("mensaje", "ERROR :" + e.getMessage());
            return new ResponseEntity<Map>(map,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
