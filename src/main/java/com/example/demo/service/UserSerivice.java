package com.example.demo.service;

import java.util.List;

import com.example.demo.model.User;

public interface UserSerivice {
	
	User createUsers(User user);
	
	void deleteUsersById(Integer idUser);
	
	User updateUsersById(User user);
	
	List<Object> getusersById(Integer idUser);
	
	List<Object> getusers();

}
