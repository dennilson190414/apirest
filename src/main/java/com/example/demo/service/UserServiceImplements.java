package com.example.demo.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.example.demo.model.User;
import com.example.demo.repository.UserRepository;

@Repository
public class UserServiceImplements implements UserSerivice {
	
	@Autowired
	private UserRepository userRepository;

	@Override
	public User createUsers(User user) {
		User us =  new User();
		
		try {
			us = userRepository.save(user);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return us;
	}

	@Override
	public void deleteUsersById(Integer id) {
		try {
			userRepository.deleteById(id);
		}catch (Exception e){
			e.printStackTrace();
		}
		
	}

	@Override
	public User updateUsersById(User user) {
		User userU = new User();
		try {
			userU = userRepository.save(user);
		}catch (Exception e){
			e.printStackTrace();
		}
		return  userU;
	}

	@Override
	public List<Object> getusersById(Integer id) {
		List<Object> getusersById = new ArrayList<>();
		try {
			getusersById =  userRepository.getusersById(id);
		}catch (Exception e){
			e.printStackTrace();
		}
			return  getusersById;
	}

	@Override
	public List<Object> getusers() {
		List<Object> getusers =  new ArrayList<>();
        try {
        	getusers = userRepository.getUserAll();
        }catch (Exception e){
            e.printStackTrace();
        }
        return getusers;
	}

}
